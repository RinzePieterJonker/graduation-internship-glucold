# Finding a relation between COPD and splice variants
COPD is a widespread disease in western countries with a prevalence of 24 on 1000, this added up to 251 million people on earth with COPD in 2016 and the World Health Organisation (WHO) only expects this number to go up due to the prevalence of smoking and the aging of the population. At the moment, there is no cure for COPD, yet there are some treatments that can be used to treat the symptoms of COPD. The problem of these treatments is that they do not work with some COPD patients. At the moment it is not known why the treatments do not work for these patients. Because of this reason, the relation between the treatment of COPD and splice variants was investigated in this study.

## Getting Started

### Instalation
The installation of this analysis pipeline can be done by downloading or cloning the scripts from the BitBucket repository, the cloning of the code can be done with the following bash command;

```bash
git clone https://RinzePieterJonker@bitbucket.org/RinzePieterJonker/graduation-internship-glucold.git
```

## Prerequisites
For this study, Python version 3.7.1 was used with Pandas version 0.9.0. The R (version 3.5.2) coding language was also used with the following packages.
|Package     | Version   | 
|:----------:|:---------:|
| EdgeR   |  3.28.0         |
| Limma   |  3.42.2        |
| TidyR  |  1.0.2        |
| ggplot2   | 3.2.1         |
| biomaRt   |  2.42.0        |
| Dplyr   | 0.8.4         |
| MatrixEQTL   | 2.3         |

## Usage
All the scripts in this repository can be run in the command line or in Rstudio if the first parameters have been changed to the location of the GLUCOLD or Indurain study, the code will only work with the data from those studies.

if the raw data of the GLUCOLD is going to be used the parameters in the __config.yaml__ need to be changed to the right parameters, then the code can run in the console without extra parameters.

## Files included
 - __Docs/__ - This folder contains the reports and the figures for the reports.
     - Report.docx - The final report of this study.
 - __Scripts/__
     - __MatrixEQTL/__
         - Indurain_MEthylationAnalysis.R - This script does the methylation analysis for the Indurain study, this can be run on a cluster.         
         - Indurain_MethPlotter.R - This script creates the figures of the results from the Indurain methylation analysis.
         - MethylationAnalysis.R - The scripts that does the methylation analysis for the GLUCOLD study, this code can also be run on a cluster.
         - MethylationPlotter.R - This script creates the figures of the results created in the function above.
     - 2ndModel.R - This script contains the pipeline that was used to find out if there were splice junctions that were effected by treatment response.
     - CountTableGenerator.py - This script was used to combine the multiple count files from the GLUCOLD study into one count table, if there was not enough memory, then it would make multiple files that could be used in R by using the created helper function located in the DataLoader.R file
     - DataLoader.R - The scripts that contains the function that could be used in R to load in the combine count table into the R environment. 
     - INDURAIN_normalisation.R - This script was used to normalize the data from the INDURAIN study.
     - MapSJtoGene.R - This script contains a function that could be used to find the gene on which a splice junction was located.
     - Normalisation and filtering.rmd - This script contains notes and test that were done in the beginning of the study.
     - NormalisationPlotter.R - This script was used to create a figure that showed if the normalization was working.
     - PipelineGeneNormalisation.R - This pipeline was used to check the effect of the treatment on the expression of splice junctions.
     - Plotter.R  - This is the code for the figures that were made form the result that was retrieved from the analysis in the pipeline above. 
     - ReturnAllGenes.R - This script was used to retrieve all the genes of the human genome. 
 - .gitignore.txt - The gitignore of this BitBucket repository
 - config.yaml - The config file for the CountTableGenerator.py script


## Author
 - __Rinze-Pieter Jonker__ (rinze.pieter.jonker@gmail.com)