#!/usr/bin/env Rscript

library(tidyr)
library(dplyr)
library(data.table)

MapSJtoGene <- function(x, genes, chunck.size = 10){
  # This function is used to map the correct ensembl gene id to the given splice junction,
  # x = the dataframe that needs the $gene column added
  # genes  = a dataframe with all the gene information
  # chunck.size = the amount of rows the function will parse at the same time
  
  # Setting starting values
  n <- nrow(x)
  done <- FALSE
  result <- NULL
  
  for (i in 0: (ceiling(n / chunck.size))) {
    
    # Checking if the code is done
    if (!done) {
      start <- i * chunck.size
      end <- start + chunck.size
      
      if (end >= n) {
        end <- n
        done <- TRUE
      }
      
      # Chunking up the data and adding the ensemble gene id
      ensemble_ids <- x[start:end, ] %>%
        tibble::rownames_to_column("rownames") %>%
        tidyr::separate(
          rownames,
          c("chr", "start", "stop", "strand"),
          sep = "_",
          remove = FALSE,
          convert = TRUE
        ) %>%
        dplyr::mutate(
          strand = ifelse(strand == 2, -1, strand),
          chr = as.character(chr)
        ) %>%
        dplyr::left_join(
          y = genes %>%
            dplyr::mutate(
              chromosome_name = as.character(chromosome_name)
            ),
          by = c(
            "chr" = "chromosome_name",
            "strand" = "strand"
          )
        ) %>%
        dplyr::filter(
          start >= start_position,
          stop <= end_position
        )
     
      if (is.null(result)) {
        result <- ensemble_ids
      } else {
        result <- rbind(result, ensemble_ids)
      }
      
    }
  }
  result <- result[!duplicated(result[,1:5]), ]
  
  x$rownames <- rownames(x)
  x <- merge(x, result[,c("rownames", "ensembl_gene_id")], by="rownames", all.x=TRUE)
  rownames(x) <- x$rownames
  x$rownames <- NULL
  x <- x %>%
    rename(
      gene = ensembl_gene_id
    )
  return(x)
}
