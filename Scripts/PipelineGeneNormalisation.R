#!/usr/bin/env Rscript

#######################################################################################################################################
# SETTING FILE PATHS                                                                                                                  #
#######################################################################################################################################

library(tidyverse)

setwd("C:/Users/rinze/Desktop/graduation-internship-glucold/Scripts")

rm(list = ls())

geneFile <- "../Data/gene_info_GRCh38.csv" # the file containing all the data for the human genome
inputFileDir <- "../Data/SpliceCountTables-Test" # the directory where the file of the GLUCOLD dataset are located
dataFile <- "../Data/after_norm_quantile.csv" # the path of the normalized data
patientFile <- "../../Data/GLUCOLDFILEgenentech_30June2016.csv" #  the patient file of the GLUCOLD study
resultDir <- "../Data/Results/" # the directory where all the result are saved
mappedFile <- "../Data/MappedData.csv" # the path where the mapped data is saved

overWrite <- FALSE
medianCutOff <- 5
minCuttOff <- 5

#######################################################################################################################################
# READING IN THE DATA                                                                                                                 #
#######################################################################################################################################
 
if (!file.exists(dataFile) | overWrite) {
  print("File Does not Exist")
  if (!file.exists(mappedFile)) {
    print("Mapped File does not exist")
    
    source("DataLoader.R")
    data <- data.loader(file_dir = inputFileDir)
    genes <- read.csv(geneFile)
    
    data <- head(data, n=50)
    
    # Mapping the Splice Junctions to the gene
    source("MapSJtoGene.R")
    data <- MapSJtoGene(data, genes, chunck.size = 250)
  
    write.csv(data, mappedFile)
  } else {
    data <- readr::read_csv(
      file = mappedFile,
      n_max = 2000
      ) %>%
      tibble::column_to_rownames("X1")
  }
  
  # Filtering out the splice junctions with to low counts
  filter <- data[, - which(names(data) == "gene")]
  filter$median <- apply(filter,  1, median)
  filter$min <- apply(filter, 1, min)
  filter$quartile <- apply(filter, 1, quantile, probs = 0.75)
  filter$keep <- filter$min > minCuttOff
  
  # Running Counts per million
  countsPerMillion <- as.data.frame(
    edgeR::cpm(
      edgeR::DGEList(counts = data[, - which(names(data) == "gene")])
    )
  )
  countsPerMillion$gene <- data$gene

  # Running normalization based on the amounts of counts for one specific gene
  after_normalization <- countsPerMillion %>%
    tibble::rownames_to_column("junction") %>%
    dplyr::select(
      junction, gene, tidyselect::everything()
    ) %>%
    tidyr::gather(
      key = "sample",
      value = "count",
      -junction,
      -gene
    ) %>%
    dplyr::filter(
      count > 0
    )  %>%
    dplyr::group_by(
      gene
    ) %>%
    dplyr::mutate(
      total.count.per.gene = sum(count) # werkt dit?
    ) %>%
    dplyr::ungroup() %>%
    dplyr::mutate(
      count = count / total.count.per.gene
    ) %>%
    dplyr::select(-total.count.per.gene) %>%
    tidyr::spread(
      key = "sample",
      value = "count"
    )
  after_normalization <- after_normalization[!is.na(after_normalization$gene),]
  after_normalization[is.na(after_normalization)] <- 0
  write.csv(after_normalization, "../Data/Prefilter_norm.csv")
  
  after_normalization <- after_normalization %>%
    dplyr::left_join(
      y = filter %>%
        tibble::rownames_to_column("junction") %>%
        dplyr::select(junction, keep),
      by = c("junction" = "junction")
    ) %>%
    dplyr::mutate(
      rowNames = paste0(junction, "_", gene)
    ) %>%
    dplyr::select(-junction, -gene) %>%
    dplyr::filter(keep) %>%
    as.data.frame()
  rownames(after_normalization) <- after_normalization$rowNames
  after_normalization$rowNames <- NULL
  after_normalization$keep <- NULL
  
  # Saving the normalized data to file
  write.csv(after_normalization, dataFile)
  
} else {
  print("File Exist")
    
  # Opening the data file
  after_normalization <- read.csv(dataFile)
  rownames(after_normalization) <- after_normalization$X
  after_normalization$X <- NULL 
  colnames(after_normalization) <- stringr::str_remove(colnames(after_normalization), "X")
  rownames(after_normalization) <- gsub( ", " , "_", rownames(after_normalization))
  rownames(after_normalization) <- paste0(after_normalization$junction, "_", after_normalization$gene)
  after_normalization$junction <- NULL
  after_normalization$gene <- NULL
}
after_normalization[is.na(after_normalization)] <- 0

patients <- read.csv(patientFile)

#######################################################################################################################################
# PREPARING THE DATA FOR THE MODEL                                                                                                    #
#######################################################################################################################################

# Setting patterns for regex
patient_pattern <- "([0-9]{4})"
time_pattern <- "[0-9]_([0-9]{1,2})"

patient <- as.factor(
  sapply(
    colnames(after_normalization),
    function(x) {
      return(patients$idnr[patients$idnr == as.numeric(str_match(x, patient_pattern)[,2])])
    }
  )
)

age <- as.numeric(
  sapply(
    colnames(after_normalization),
    function(x){
      return(patients$agev1[patients$idnr == as.numeric(str_match(x, patient_pattern)[,2])])
    }
  )
)

gender <- as.factor(
  sapply(
    colnames(after_normalization),
    function(x){
      return(patients$gender[patients$idnr == as.numeric(str_match(x, patient_pattern)[,2])])
    }
  )
)

smokingstatus <- sapply(
  colnames(after_normalization),
  function(x){
    patient_id <- as.numeric(str_match(x, patient_pattern)[,2])
    visit <- str_match(x, time_pattern)[,2]
    
    smoking_status <- dplyr::case_when(
      visit == "1" ~ patients$smokingstatusv1[patients$idnr == patient_id],
      visit == "3" ~ patients$smokingstatusv3[patients$idnr == patient_id],
      visit == "11" ~ patients$smokingstatusv11[patients$idnr == patient_id]
    )
    if (is.na(smoking_status)){
      smoking_status <- patients$smokingstatusv1[patients$idnr == patient_id]
    }
    return(smoking_status)
  }
)

treatment <- as.factor(
  sapply(
    colnames(after_normalization),
    function(x){
      treatment <- patients$treatment[patients$idnr == as.numeric(str_match(x, patient_pattern)[,2])]
      visit <- str_match(x, time_pattern)[,2]
      return(
       dplyr::case_when(
         treatment == "Fluticasone 30 months" | treatment == "Fluticasone/Salmeterol" ~ "Yes",
         treatment == "Placebo" ~ "Placebo",
         treatment == "Fluticasone 6 months" & visit == 11 ~ "ICS",
         treatment == "Fluticasone 6 months" ~ "Yes"
        )
      )
    }
  )
)

time <-as.factor(
  sapply(
    colnames(after_normalization),
    function(x){
      time <- str_match(x, time_pattern)[,2]
      
      return(
        dplyr::case_when(
          time == "1" ~ 0,
          time == "3" ~ 6,
          time == "11" ~ 30
        )
      )
    }
  )
)

treatment.time <- paste0(treatment, ".", time)
block <- c(as.character(colnames(data)))
rownames(after_normalization) <- gsub( ", " , "_", rownames(after_normalization))

#######################################################################################################################################
# CREATING AND RUNNING THE MODELS                                                                                                     #
#######################################################################################################################################

# Runnging the model
library(limma)

design <- model.matrix( ~ 0 + treatment.time + age + gender + smokingstatus,
                        block = patient)
fit <- limma::lmFit(after_normalization, design)

# Setting the interaction terms
contrast.matrix <- limma::makeContrasts(
  baseline_short = (treatment.timeYes.6 - treatment.timePlacebo.6),
  baseline_long = (treatment.timeYes.30 - treatment.timePlacebo.30),
  levels = colnames(coef(fit))
)

tmp <- limma::contrasts.fit(fit, contrast.matrix)
tmp <- limma::eBayes(tmp, 0.01)

# Saving the reslts
write.csv(limma::topTable(tmp, n = Inf, coef = "baseline_short"), paste0(resultDir, "baseline_short_quantile.csv"))
write.csv(limma::topTable(tmp, n = Inf, coef = "baseline_long"), paste0(resultDir, "baseline_long_quantile.csv"))







