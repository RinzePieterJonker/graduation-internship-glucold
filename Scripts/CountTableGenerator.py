#!/usr/bin/env python3

"""
This code is used to generate the junction count data from the junction read tables that where made by the STAR aligner,
This script uses a lot of memory, so this may take a while

usage:
This script does not need any arguments, only the FILE_PATH global needs to be changed

python3 CountTableGenerator.py
"""

# IMPORTS
import gzip
import time
import os
import glob
import csv
import re
import yaml

import pandas as pd


# METADATA
__author__ = "Rinze-Pieter Jonker"
__status__ = "Working"
__version__ = 1.0


# GLOBALS
with open("../config.yaml", "r") as f:
    config = yaml.load(f)
    FILE_PATH = config["filepath"]
    CHROMOSOMES = config["chromosomes"]
    SAM_ID_FILE = config["sam_id_file"]


# CODE
class CountTableGenerator:
    """
    This class was made to combine the sample junction files and to create a splice junction count file

    :param junction_directory: String, the path to the location with all the sample files
    :param sam_id_file: String, the path to the file with all the patient_ids and sample_ids
    :param save_location: String, the path to the save location, Default = '../Data/SpliceCountTables/'"
    """
    def __init__(self, junction_directory, sam_id_file, save_location="../Data/SpliceCountTables/"):
        self.junction_directory = junction_directory
        self.sam_id_dictionary = self.generate_sam_id_dict(sam_id_file)
        self.save_location = save_location

    def generate_sam_id_dict(self, sam_id_file):
        """
        This function generates a dictionary that can be used to link the Splice junction files to the patient id

        :param sam_id_file: string, the path to where the file with all the sam ids are saved
        :return: a dictionary with the filename (without SJ.out.tab.gz) as the key and the patient id as the value
        """
        sam_id_dict = {}
        with open(sam_id_file, "r") as fin:
            for count, line in enumerate(fin):
                # Skipping the first line since this is a header
                if count != 0:
                    line = line.split(",")

                    # Generating the filename out the sam id file and giving it the right format
                    id = "{}_{}_0{}_{}_L{}_{}".format(line[4], line[2], line[5], line[6], line[1], line[9])

                    # Removing the extra 0 in some of the patient ids
                    if re.search(pattern=r"_[0]", string=line[10]):
                        line[10] = line[10][:5] + line[10][-1]

                    patient = line[10]

                    # Adding a number at the end to keep al the ids unique since there are more then one sample for
                    # each patient
                    # patient_count = 0
                    # while patient in sam_id_dict.values():
                    #     patient_count += 1
                    #     patient = "{}_{}".format(line[10], patient_count)

                    sam_id_dict[id] = patient
        return sam_id_dict

    def retrieve_junction_data(self, file_name, patient):
        """
        This function retrieves all the count data from a splice junction file

        :param file_name: String, the path to the splice junction file
        :param patient: String, the id of the patient
        :return: tupple with 3 pandas DataFrame, one containing only the counts form junctions where the chromosome is
                 known, one with only the junctions where the chormosomes are not known and one where all the data is
                 saved
        """
        # Creating empty lists where the data can be saved
        known = []
        contig = []
        all = []

        # Opening the file while it is compressed
        with gzip.open(file_name, "r") as fin:
            for line in fin:
                # Converting the line from 'bytes' data type to 'list' and removing the new line
                old_line = line.decode("utf-8").strip("\n").split("\t")

                # Adding one to the count
                line = ["{}_{}_{}_{}".format(old_line[0], old_line[1], old_line[2], old_line[3]), int(old_line[6]) + 1]

                # Saving the line to the right data frame, the contig data frame contains the junctions where the
                # chromosome is not known, The chromosome is the first number the identifier
                if old_line[0] in CHROMOSOMES:
                    known.append(line)
                else:
                    contig.append(line)
                all.append(line)

        known = pd.DataFrame(known, columns=["junction_id", patient])
        contig = pd.DataFrame(contig, columns=["junction_id", patient])
        all = pd.DataFrame(all, columns=["junction_id", patient])
        return known, contig, all

    def parse_file(self, file_name, use_contig=2):
        """
        This function manages every function that needs to be preformed on one file

        :param file_name: String, the path to the splice junction file
        :param use_contig: Int, which DataFrame needs to be used, 0 for only junctions where the chromosome is known,
                                1 for only junctions where the chromsome is not known and 2 for all the data
        :return: pandas DataFrame, the counts for one patient
        """
        sam_id = os.path.basename(file_name).strip("SJ.out.tab.gz")
        patient = self.sam_id_dictionary[sam_id]

        return self.retrieve_junction_data(file_name, patient)[use_contig]

    def save_counts(self, count_table, save_name=None):
        """
        This function saves the count table

        :param count_table: pandas DataFrame, this is the data
        :param save_name: String, the path to the save location
        """
        # Checking if a filename is given, if none is given it will use a default
        if save_name is None:
            save_name = self.save_location + "JunctionCountTable1.csv"

        # Creating a new name if the current name is already taken
        save_count = 1
        while os.path.exists(save_name):
            save_name = self.save_location + "JunctionCountTable{}.csv".format(save_count)
            save_count += 1

        count_table.to_csv(save_name)
        print(" [SAVING] saved count table to '{}'".format(save_name))

    def run(self, break_point=None):
        """
        This function will run the object and return the full count table

        :param break_point: Int, the amount of files it will take out of the file list, default = None
        """
        # Generating a list with all the files
        file_list = glob.glob(FILE_PATH + "*" + ".gz")
        count_table = pd.DataFrame()

        for count, file in enumerate(file_list):
            # Parsing each file
            counts = self.parse_file(file_name=file)

            # Try to merge it with the existing count file, if this is not possible due to memory issues, it will save
            # the existing table and make a new table
            try:
                if count_table.empty:
                    count_table = counts
                else:
                    count_table = pd.merge(left=count_table,
                                           right=counts,
                                           on=["junction_id", "junction_id"],
                                           how="outer")
            except MemoryError:
                print(" [ERROR] not enough memory, saving current DataFrame and creating a new file")

                self.save_counts(count_table)
                count_table = counts

            print(os.path.basename(file))

            # if a breakpoint is given it will use this to create a smaller file with still all the junctions
            if break_point is not None and count > break_point:
                break

        self.save_counts(count_table)
        pass


def main():
    count_table_generator = CountTableGenerator(sam_id_file=SAM_ID_FILE,
                                                junction_directory=FILE_PATH)

    with open("../Data/PatientToSamID.csv", "w", newline="") as csvfile:
        writer = csv.writer(csvfile)
        for key, value in count_table_generator.sam_id_dictionary.items():
            writer.writerow([key, value])

    count_table_generator.run()
    return 0


if __name__ == "__main__":
    print(" > Code started at {}".format(time.asctime()))
    start = time.time()
    main()
    print("\n > Code finished, took {} seconds".format(time.time() - start))
