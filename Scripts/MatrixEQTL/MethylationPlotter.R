
library(tidyverse)

setwd("C:/Users/rinze/Desktop/graduation-internship-glucold/Scripts/MatrixEQTL")
rm(list = ls())

base.dir <- "J:/DataIndurain/"
save.dir <- "img/"
n.max <- Inf

#########################################################################################################################################
# PREPARING THE DATA                                                                                                                    #
#########################################################################################################################################
# Reading in the files made from the methylation analysis and removing the X from the columns

GeneInformation <- readr::read_csv(
  "C:/Users/rinze/Desktop/graduation-internship-glucold/Data/gene_info_GRCh37.csv"
) %>%
  dplyr::select(hgnc_symbol, ensembl_gene_id)

CisEqtls <- read.table(
  paste0(base.dir, "MethylationResults_Corneel.txt"),
  header = TRUE,
  nrows = n.max
  ) %>%
  tidyr::separate(
    col = gene,
    sep = "_", 
    remove = FALSE,
    into = c("chr", "start", "stop", "strand", "ensembl")
    )

# p.adjust(pvalue, method = "fdr", n = N)
CisEqtls <- CisEqtls %>%
  dplyr::select(snps, gene, pvalue, ensembl) %>%
  dplyr::arrange(ensembl, pvalue) %>%
  dplyr::group_by(ensembl) %>%
  dplyr::mutate(
    N = n()
    ) %>%
  dplyr::ungroup() %>%
  dplyr::select(snps, gene, N) %>%
  dplyr::left_join(
    y = CisEqtls,
    by = c("snps" = "snps",
           "gene" = "gene")
  )
CisEqtls <- head(CisEqtls[order(CisEqtls$FDR), ], n =10000)

adj <- c()
for (row in 1:nrow(CisEqtls)) {
  adj <- append(
    adj,
    p.adjust(
      p = CisEqtls[row, "pvalue"],
      method = "fdr",
      n = CisEqtls[row, "N"]
    )
  )
}
CisEqtls$adj <- adj


CisEqtls$hgnc <- sapply(
  CisEqtls$ensembl,
  function(x) {
    hgnc <- GeneInformation$hgnc_symbol[GeneInformation$ensembl_gene_id == stringr::str_extract(x, "ENSG[0-9]*")]
    hgnc <- hgnc[!duplicated(hgnc)]
    return(hgnc)
    }
  )

# ENSG00000165092

masterTable <- readr::read_csv(paste0(base.dir, "master_table_min.txt"))

select.columns.in.order <- function(dataframe, columns) {
  dataframe[, columns]
}

SpliceJunctions <- readr::read_csv(
  "../../Data/after_norm_min.csv",
  n_max = n.max) %>%
  select.columns.in.order(c("X1", masterTable$SampleID)) %>%
  tibble::as_tibble() %>%
  dplyr::mutate(X1 = stringr::str_replace(X1, ", ", "_"))

geFile <- readr::read_delim(
  file = paste0(base.dir, "GE_min.txt"),
  delim = "\t"
)

snpFile <- readr::read_delim(
  file = paste0(base.dir, "SNP_min.txt"),
  delim = "\t"
)


#########################################################################################################################################
# CREATING THE PLOTS                                                                                                                    #
#########################################################################################################################################



# Filtering out only the significant sites
to.plot <- CisEqtls %>%
  dplyr::filter(adj < 0.05) %>%
  head(n = 50)

write.csv(to.plot ,
          "header_EQTLS.csv")

# creating a scatterplot for each significant gene
count <- 0
for (i in 1:nrow(to.plot)) {
  count <- count + 1
 
  gene <- as.character(to.plot$gene[i]) # Splice Junctions
  snp <- as.character(to.plot$snps[i]) # wat uit de methylatie komt
  hgnc <- as.character(to.plot$hgnc[i])
  
  fdr <- formatC(
    as.numeric(to.plot[i, "FDR"]),
    format = "e",
    digits = 3) 
  
  dataToPlot <- geFile %>%
    dplyr::filter(id == gene) %>%
    tidyr::gather(key = "SampleID",
                  value = "geExpression",
                  -id) %>%
    dplyr::mutate(SampleID = as.character(SampleID)) %>%
    dplyr::rename(ExpressionID = "id") %>%
    dplyr::left_join(
      y = masterTable %>%
        dplyr::select(
          MethylID,
          idnr,
          treatment,
          Visit,
          SampleID,
          smokingStatus
        ),
      by = c("SampleID" = "SampleID")
    ) %>%
    dplyr::mutate(
      Smoking_Status = 
        dplyr::case_when(
          smokingStatus == "ja" ~ "Yes",
          TRUE ~ "No"
        )
    ) %>%
    dplyr::left_join(
      y = snpFile %>%
        dplyr::filter(id == snp) %>%
        tidyr::gather(key = "SampleID",
                      value = "snpValue",
                      -id) %>%
        dplyr::rename(snpID = "id") %>%
        dplyr::mutate(snpValue = as.numeric(snpValue)),
      by = c("SampleID" = "SampleID")
      ) %>%
    dplyr::mutate(Visit = as.character(Visit))
  
  # Correlation plot between methlation and the splice junction expression
  ggplot2::ggsave(
    filename = paste0(save.dir, fdr," ", count, " Correlation plot for ", hgnc, ".png"), 
    plot = ggplot2::ggplot(
      dataToPlot,
      ggplot2::aes(x = geExpression, y = snpValue)
      ) + 
      ggplot2::geom_point(
        mapping = ggplot2::aes(color = Smoking_Status)
      ) +
      ggplot2::geom_smooth(
        method = 'lm') + 
      ggplot2::labs(
        title = paste0(
          hgnc,
          " at baseline (N = ",
          dim(masterTable)[1],
          ", FDR = ",
          fdr,
          ")"),
        x = gene,
        y = snp
        )  + 
      ggthemes::theme_clean() + 
      ggplot2::theme(legend.position = "bottom")
    
    )
}


  
