
# Setting Parmeters for the analysis
setwd("C:/Users/rinze/Desktop/graduation-internship-glucold/Scripts/MatrixEQTL")

library(tidyverse)
library(MatrixEQTL)

rm(list = ls())

base.dir <- "" # Where all the data is located

indurainCountFile <- "../../Data/INDURAIN/count.data.csv" # The count file of the indurain study
indurainCountPatientFile <- "../../Data/INDURAIN/database.csv" # the patient file of the indurain study

geneInfoFile <- "../../Data/gene_info_GRCh37.csv" # the file with all the data of the human genome

indurainMethylationFile <- "J:/NORM & INDURAIN - Methylation/methylationData.male.female.csv" # the methylation data of the indurain study
indurainMehtylationPatientFile <- "J:/NORM & INDURAIN - Methylation/pd.male.female.csv" # the patient table that was used with the methylation data of the indurain study
bad_cpg_sites_file <- "J:/NORM & INDURAIN - Methylation/1-s2.0-S221359601630071X-mmc2.txt" # the file with all the cpg sites that shouldn't be used

dataDir <- "DataIndurain/" # where the data of the indurain study is going to be saved
snpLocInformation <- "J:/NORM & INDURAIN - Methylation/CpG information/MethylationEPI.C.hg37.csv" # the file with all the data of the cpg sites

covFile <- paste0(dataDir, "covariates.txt") # The path where the covariates file is going to be saved
GEFile <- paste0(dataDir, "GE.txt") # the path were the GE file is going to be saved
masterTableFile <- paste0(dataDir, "master_table.txt") # the path where the master table is going to be saved
geneLocFile <- paste0(dataDir, "geneloc.txt") # the path where the Gene Location file is going to be saved
snpFile <- paste0(dataDir, "SNP.txt") # the path where the methylation data is going to be saved
snpLocFile <- paste0(dataDir, "snpsloc.txt") # the path where the mehtylation location data is going to be saved
resultFile <- paste0(dataDir, "MethylationResults.txt") # the path were the results are going to be saved

genes <- read.csv(geneInfoFile)

select.columns.in.order <- function(dataframe, columns) {
  dataframe[, columns]
}

interesstingEnsembl <- c("ENSG00000117983", "ENSG00000215182")

########################################################################################################################################
# READING IN THE DATA                                                                                                                  #
########################################################################################################################################
# indurainCount <- read.csv(
#   indurainCountFile,
#   nrows = n_max
#   ) %>%
#   dplyr::mutate(
#     X = gsub("m", "2", X),
#     X = gsub("p", "1", X)
#     ) %>%
#   tibble::column_to_rownames("X")
# colnames(indurainCount) <- gsub("^X|_$", "", colnames(indurainCount))
# colnames(indurainCount) <- gsub("_", "-", colnames(indurainCount))
# 
# source("../MapSJtoGene.R")
# 
# indurainCount <- MapSJtoGene(
#   x = indurainCount,
#   genes = genes,
#   chunck.size = 250
# )
# 
# indurainCount <- indurainCount %>%
#   tibble::rownames_to_column("X") %>%
#   dplyr::mutate(X = paste0(X, "_", gene)) %>%
#   dplyr::select(-gene)

# Reading in the results from the normalisation
indurainCount <- read.csv("../../Data/after_norm_INDURAIN.csv")
colnames(indurainCount) <- gsub("^X|_$", "", colnames(indurainCount))
colnames(indurainCount) <- gsub("_", "-", colnames(indurainCount))
colnames(indurainCount)[1] <- "X"

# Reading in the patient table and creating a master table 
masterTable <- read.csv(indurainCountPatientFile) %>%
  dplyr::select(X, link.id, rnaseq.id, gender, age, currentsmoking) %>%
  dplyr::mutate(
    rnaseq.id = gsub("^X|_$|_NA", "", rnaseq.id),
    rnaseq.id = gsub("_", "-", rnaseq.id)
  ) %>%
  dplyr::left_join(
    y = read.csv(indurainMehtylationPatientFile) %>%
      dplyr::select(person, barcode, row.names),
    by = c("rnaseq.id" = "person")
  )

# Reading in the file with the bad cpg sites 
bad_cpg_sites <- readr::read_csv(
  bad_cpg_sites_file,
  col_names = FALSE)

# Reading in the methylation data
methylationData <- readr::read_csv(
  indurainMethylationFile
  )
methylationData <- methylationData[!methylationData$row.names %in% bad_cpg_sites$X1, ]

# Removing useless rows from the mastertable
masterTable <- masterTable[complete.cases(masterTable), ]
masterTable <- masterTable[masterTable$rnaseq.id %in% colnames(indurainCount), ]
masterTable <- masterTable[masterTable$barcode %in% colnames(methylationData), ]

masterTable <- masterTable[order(masterTable$rnaseq.id), ]

########################################################################################################################################
# CREATING THE METHYLATION FILES                                                                                                       #
########################################################################################################################################

# Preparing the count data
indurainCount <- indurainCount %>%
  tidyr::separate(
    col = X,
    sep = "_", 
    remove = FALSE,
    into = c("chr", "start", "stop", "strand", "ensembl")
  ) %>%
  dplyr::filter(
    ensembl %in% interesstingEnsembl
  ) %>%
  dplyr::select(-chr, -start, -stop, -strand, -ensembl) %>%
  dplyr::rename(id = X)

# Covariates file
covariates <- masterTable %>%
  dplyr::select(rnaseq.id, gender, age, currentsmoking) %>%
  dplyr::mutate(
    currentsmoking = dplyr::case_when(
      currentsmoking == "smoker" ~ 1,
      currentsmoking == "nonsmoker" ~ 0
    ),
    gender = dplyr::case_when(
      gender == "male" ~ 1,
      gender == "female" ~ 0
    )
  ) %>%
  dplyr::mutate(age = round(as.numeric(gsub(",", ".", levels(age)[age])))) %>%
  t() %>%
  as.data.frame() 

readr::write_delim(
  covariates %>%
    tibble::rownames_to_column("X"),
  path = covFile,
  delim = "\t",
  col_names = FALSE
)

# GeneExpression file
GE <- indurainCount %>%
  select.columns.in.order(c("id", masterTable$rnaseq.id))

# GeneExpressionLoc
GeneLoc <- indurainCount[,c(1, 2)] %>%
  tidyr::separate(
    id,
    into = c("chr", "s1", "s2", "strand", "ensembl.id"),
    sep = "_",
    remove = FALSE,
    convert = TRUE
  ) %>%
  dplyr::select(id, chr, s1, s2)

readr::write_delim(
  GE,
  path = GEFile,
  delim = "\t"
)

readr::write_delim(
  GeneLoc,
  path = geneLocFile,
  delim = "\t"
)

# SNP
translate_frame <- data.frame(
  name = colnames(methylationData )
) %>%
  dplyr::left_join(
    y = masterTable%>%
      dplyr::select(barcode, rnaseq.id),
    by = c("name" = "barcode")
  ) 

colnames(methylationData) <- translate_frame$rnaseq.id
colnames(methylationData)[1] <- "id"
methylationData <- methylationData[, !is.na(colnames(methylationData))] 

methylationData <- select.columns.in.order(methylationData, c("id", masterTable$rnaseq.id))

readr::write_delim(
  methylationData,
  path = snpFile,
  delim = "\t"
)
cpg_information <- readr::read_csv(snpLocInformation)
cpg_information <- cpg_information[complete.cases(cpg_information), ]

readr::write_delim(
  cpg_information,
  path = snpLocFile,
  delim = "\t"
)

readr::write_delim(
  masterTable,
  path = masterTableFile,
  delim = "\t"
)

########################################################################################################################################
# RUNNING THE ANALYSIS                                                                                                                 #
########################################################################################################################################

# Output file name
output_file_name_cis = tempfile();
output_file_name_tra = tempfile();

# Only associations significant at this level will be saved
pvOutputThreshold_cis = 5e-2;
pvOutputThreshold_tra = 0; #1e-2;

# Error covariance matrix
# Set to numeric() for identity.
errorCovariance = numeric();
# errorCovariance = read.table("Sample_Data/errorCovariance.txt");

output_file_name_cis = tempfile();
output_file_name_tra = tempfile();

# Only associations significant at this level will be saved
pvOutputThreshold_cis = 5e-2;
pvOutputThreshold_tra = 0; #1e-2;

# Error covariance matrix
# Set to numeric() for identity.
errorCovariance = numeric();
# errorCovariance = read.table("Sample_Data/errorCovariance.txt");

# Distance for local gene-SNP pairs
cisDist = 1e6; # Usually is 1M bp, but for the eQTL distribution plot, it would be 2M bp.

## Load genotype data

snps = SlicedData$new();
snps$fileDelimiter = "\t";      # the TAB character
snps$fileOmitCharacters = "NA"; # denote missing values;
snps$fileSkipRows = 1;          # one row of column labels
snps$fileSkipColumns = 1;       # one column of row labels
snps$fileSliceSize = 2000;      # read file in slices of 2,000 rows
snps$LoadFile(snpFile);

## Load gene expression data

gene = SlicedData$new();
gene$fileDelimiter = "\t";      # the TAB character
gene$fileOmitCharacters = "NA"; # denote missing values;
gene$fileSkipRows = 1;          # one row of column labels
gene$fileSkipColumns = 1;       # one column of row labels
gene$fileSliceSize = 2000;      # read file in slices of 2,000 rows
gene$LoadFile(GEFile);

## Load covariates

cvrt = SlicedData$new();
cvrt$fileDelimiter = "\t";      # the TAB character
cvrt$fileOmitCharacters = "NA"; # denote missing values;
cvrt$fileSkipRows = 1;          # one row of column labels
cvrt$fileSkipColumns = 1;       # one column of row labels
if(length(covFile)>0) {
  cvrt$LoadFile(covFile);
}

## Run the analysis
snpspos = read.table(snpLocFile, header = TRUE, stringsAsFactors = FALSE);
genepos = read.table(geneLocFile, header = TRUE, stringsAsFactors = FALSE);

useModel <- modelLINEAR

me = Matrix_eQTL_main(
  snps = snps,
  gene = gene,
  cvrt = cvrt,
  output_file_name = output_file_name_tra,
  pvOutputThreshold = pvOutputThreshold_tra,
  useModel = useModel,
  errorCovariance = errorCovariance,
  verbose = TRUE,
  output_file_name.cis = output_file_name_cis,
  pvOutputThreshold.cis = pvOutputThreshold_cis,
  snpspos = snpspos,
  genepos = genepos,
  cisDist = cisDist,
  pvalue.hist = "qqplot",
  min.pv.by.genesnp = FALSE,
  noFDRsaveMemory = FALSE);

unlink(output_file_name_tra);
unlink(output_file_name_cis);

# reading in the results and preparing it for plotting with ggplot2
results <- me$cis$eqtls %>%
  dplyr::mutate(
    Significant =
      dplyr::case_when(
        FDR < 0.05 ~ "Yes",
        TRUE ~ "No"),
    logPvalue = -log10(pvalue)
  ) %>%
  tidyr::separate(
    col = gene,
    into = c("chr", "start", "stop", "strand", "ensembl"),
    convert = TRUE,
    sep = "_"
  ) %>%
  dplyr::mutate(StartStop = paste0(start, "-", stop))

# plotting a figure for each interesting cpg site from each interesting gene
results <- results[order(results$start),]
for (gene in interesstingEnsembl) {
  toPlotGene <- results %>%
    dplyr::filter(
      ensembl == gene
    )
  for (snps in as.character(toPlotGene$snps[toPlotGene$Significant == "Yes"])) {
    
    toPlot <- toPlotGene[toPlotGene$snps == snps, ]
    toPlot <- toPlot[order(toPlot$start),]
    toPlot$Exon <- paste0("Intron ", seq(from = 1, to = nrow(toPlot)))
    toPlot$Exon <- factor(toPlot$Exon, levels = toPlot$Exon)
   
    
    ggplot2::ggsave(
      plot = ggplot2::ggplot(
        data = toPlot,
        ggplot2::aes(
          y = logPvalue,
          x = Exon
          )
        ) + 
        ggplot2::geom_bar(stat = "identity", ggplot2::aes(fill = Significant)) +
        ggplot2::theme_bw() +
        ggplot2::labs(x = "")+
        ggplot2::theme(
          axis.text.x = ggplot2::element_text(
            angle = 70,
            hjust = 1,
            size = 7.5
            ),
          legend.position = "bottom"
        ),
      file = paste0("img/", "effect of ", snps, "on the splice junctions of ", genes$hgnc_symbol[genes$ensembl_gene_id == gene][1], ".jpg"),
      height = 7,
      width = 11
      )
  }
}

# Saving all the results
readr::write_delim(
  me$cis$eqtls,
  path = paste0(base.dir, "MethylationResults_min.txt"),
  delim = "\t"
)

